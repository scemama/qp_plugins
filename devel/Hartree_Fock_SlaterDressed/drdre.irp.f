!*****************************************************************************
program DrDre

  implicit none

  double precision              :: expGau,expSla
  double precision              :: cGau(3),cSla(3)
  integer                       :: aGau(3)
  double precision              :: cNuc(3)
  double precision              :: ZNuc

! Intro

  write(*,*) 
  write(*,*) '****************************'
  write(*,*) '*          Dr Dre          *'
  write(*,*) '* Dressing integral module *'
  write(*,*) '****************************'
  write(*,*) 

!
! Input 
!

! Exponent of the Gaussian and Slater functions

  expGau = 2d0
  expSla = 3d0

! Coordinates of Gaussian center

  cGau(1) = 1d0
  cGau(2) = 2d0
  cGau(3) = 3d0

!  cGau(1) = 0d0
!  cGau(2) = 0d0
!  cGau(3) = 0d0

! Coordinates of Slater center

  cSla(1) = -0.5d0
  cSla(2) = 0d0
  cSla(3) = 1d0

!  cSla(1) = 0d0
!  cSla(2) = 0d0
!  cSla(3) = 0d0

! Angular momentum of Gaussian function (optinal)

  aGau(1) = 0
  aGau(2) = 0
  aGau(3) = 0

! Charge and coordinates of the nucleus

  ZNuc = 4d0
  cNuc(1) = 0.
  cNuc(2) = 0.
  cNuc(3) = 0.

! Printing input variables
  write(*,'(A19)') 'Gaussian function:'
  write(*,'(A19)') '------------------'
  write(*,'(A19,1X,F16.10)') 'Exponent:',expGau
  write(*,'(A19,1X,F16.10,F16.10,F16.10)') 'Center coordinates:',cGau
!  write(*,'(A19,1X,I16,I16,I16)') 'Angular momentum:',aGau
  write(*,*)
  write(*,'(A19)') 'Slater function:'
  write(*,'(A19)') '----------------'
  write(*,'(A19,1X,F16.10)') 'Exponent:',expSla
  write(*,'(A19,1X,F16.10,F16.10,F16.10)') 'Center coordinates:',cSla
  write(*,*)
  write(*,'(A19)') 'Nuclear center:'
  write(*,'(A19)') '---------------'
  write(*,'(A19,1X,F16.10)') 'Nuclear charge:',Znuc
  write(*,'(A19,1X,F16.10,F16.10,F16.10)') 'Center coordinates:',cNuc
  write(*,*)

! Compute overlap integral between Gaussian and Slater

  write(*,*) 'Compute mixed overlap integrals for dressing'
  write(*,*) '--------------------------------------------'
  write(*,*)
  call GauSlaOverlap(expGau,cGau,aGau,expSla,cSla)
  write(*,*)

! Compute kinetic energy integral between Gaussian and Slater

  write(*,*) 'Compute mixed kinetic energy integrals for dressing'
  write(*,*) '---------------------------------------------------'
  write(*,*)
  call GauSlaKinetic(expGau,cGau,aGau,expSla,cSla)
  write(*,*)

! Compute nuclear attraction integral between Gaussian and Slater

!  write(*,*) 'Compute mixed nuclear attraction integral for dressing'
!  write(*,*)
!  call GauSlaNuclear(expGau,cGau,aGau,expSla,cSla,ZNuc,cNuc)

end 
!*****************************************************************************

