  program print_matrix_element
       use bitmasks
       implicit none

       integer(bit_kind) :: det_i(N_int,2)
       integer(bit_kind) :: det_j(N_int,2)

       double precision  :: hij

       integer           :: i,j

       print *,  'Determinant <i| ?'
       read(*,*) i
       det_i(1:N_int, 1:2) = psi_det(1:N_int, 1:2, i)

       print *,  'Determinant |j> ?'
       read(*,*) j
       det_j(1:N_int, 1:2) = psi_det(1:N_int, 1:2, j)

       call i_H_j(det_i, det_j, N_int, hij)
       print *,  '<i|H|j> = ', hij

     end

