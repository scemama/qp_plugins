subroutine union_aux_ao_to_union_aux_mo(A_ao,LDA_ao,A_mo,LDA_mo)
  implicit none
  BEGIN_DOC
  ! Transform A from the AO basis to the MO basis
  !
  ! Ct.A_ao.C
  END_DOC
  integer, intent(in)            :: LDA_ao,LDA_mo
  double precision, intent(in)   :: A_ao(LDA_ao,union_aux_ao_num)
  double precision, intent(out)  :: A_mo(LDA_mo,union_aux_mo_tot_num)
  double precision, allocatable  :: T(:,:)
  
  allocate ( T(union_aux_ao_num,union_aux_mo_tot_num) )
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: T
  
  call dgemm('N','N', union_aux_ao_num, union_aux_mo_tot_num, union_aux_ao_num,&
      1.d0, A_ao,LDA_ao,                                             &
      union_aux_mo_coef, size(union_aux_mo_coef,1),                  &
      0.d0, T, size(T,1))
  
  call dgemm('T','N', union_aux_mo_tot_num, union_aux_mo_tot_num, union_aux_ao_num,&
      1.d0, union_aux_mo_coef,size(union_aux_mo_coef,1),             &
      T, union_aux_ao_num,                                           &
      0.d0, A_mo, size(A_mo,1))
  
  deallocate(T)
end

subroutine union_aux_mo_to_union_aux_ao(A_mo,LDA_mo,A_ao,LDA_ao)
  implicit none
  BEGIN_DOC
  ! Transform A from the MO basis to the AO basis
  !
  ! (S.C).A_mo.(S.C)t
  END_DOC
  integer, intent(in)            :: LDA_ao,LDA_mo
  double precision, intent(in)   :: A_mo(LDA_mo,union_aux_mo_tot_num)
  double precision, intent(out)  :: A_ao(LDA_ao,union_aux_ao_num)
  double precision, allocatable  :: T(:,:)
  
  allocate ( T(union_aux_mo_tot_num,union_aux_ao_num) )
  
  call dgemm('N','T', union_aux_mo_tot_num, union_aux_ao_num, union_aux_mo_tot_num,&
      1.d0, A_mo,size(A_mo,1),                                       &
      S_union_aux_mo_coef, size(S_union_aux_mo_coef,1),              &
      0.d0, T, size(T,1))
  
  call dgemm('N','N', union_aux_ao_num, union_aux_ao_num, union_aux_mo_tot_num,&
      1.d0, S_union_aux_mo_coef, size(S_union_aux_mo_coef,1),        &
      T, size(T,1),                                                  &
      0.d0, A_ao, size(A_ao,1))
  
  deallocate(T)
end

BEGIN_PROVIDER [ double precision, S_union_aux_mo_coef, (union_aux_ao_num, union_aux_mo_tot_num) ]
  implicit none
  BEGIN_DOC
  ! Product S.C where S is the overlap matrix in the AO basis and C the union_aux_mo_coef matrix.
  END_DOC
  
  call dgemm('N','N', union_aux_ao_num, union_aux_mo_tot_num, union_aux_ao_num,&
      1.d0, union_aux_ao_overlap,size(union_aux_ao_overlap,1),       &
      union_aux_mo_coef, size(union_aux_mo_coef,1),                  &
      0.d0, S_union_aux_mo_coef, size(S_union_aux_mo_coef,1))
  
END_PROVIDER

