
BEGIN_PROVIDER [ double precision, union_aux_mo_overlap,(union_aux_mo_tot_num,union_aux_mo_tot_num)]
  implicit none
  integer :: i,j,n,l
  double precision :: f
  integer :: lmax
  lmax = (union_aux_ao_num/4) * 4
  !$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(NONE) &
  !$OMP  PRIVATE(i,j,n,l) &
  !$OMP  SHARED(union_aux_mo_overlap,union_aux_mo_coef,union_aux_ao_overlap, &
  !$OMP    union_aux_mo_tot_num,union_aux_ao_num,lmax)
  do j=1,union_aux_mo_tot_num
   do i= 1,union_aux_mo_tot_num
    union_aux_mo_overlap(i,j) = 0.d0
    do n = 1, lmax,4
     do l = 1, union_aux_ao_num
      union_aux_mo_overlap(i,j) = union_aux_mo_overlap(i,j) + union_aux_mo_coef(l,i) * &
           ( union_aux_mo_coef(n  ,j) * union_aux_ao_overlap(l,n  )  &
           + union_aux_mo_coef(n+1,j) * union_aux_ao_overlap(l,n+1)  &
           + union_aux_mo_coef(n+2,j) * union_aux_ao_overlap(l,n+2)  &
           + union_aux_mo_coef(n+3,j) * union_aux_ao_overlap(l,n+3)  )
     enddo
    enddo
    do n = lmax+1, union_aux_ao_num
     do l = 1, union_aux_ao_num
      union_aux_mo_overlap(i,j) = union_aux_mo_overlap(i,j) + union_aux_mo_coef(n,j) * union_aux_mo_coef(l,i) * union_aux_ao_overlap(l,n)
     enddo
    enddo
    if (dabs(union_aux_mo_overlap(i,j)) < 1.d-12) then
      union_aux_mo_overlap(i,j) = 0.d0
    endif
   enddo
  enddo
  !$OMP END PARALLEL DO
END_PROVIDER

