BEGIN_PROVIDER [double precision, union_aux_mo_kinetic_integral, (union_aux_mo_tot_num,union_aux_mo_tot_num)]
  implicit none
  BEGIN_DOC
  !  Kinetic energy integrals in the MO basis
  END_DOC
  
  call union_aux_ao_to_union_aux_mo(                                 &
      union_aux_ao_kinetic_integral,                                 &
      size(union_aux_ao_kinetic_integral,1),                         &
      union_aux_mo_kinetic_integral,                                 &
      size(union_aux_mo_kinetic_integral,1)                          &
      )

END_PROVIDER

