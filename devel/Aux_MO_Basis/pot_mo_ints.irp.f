BEGIN_PROVIDER [double precision, union_aux_mo_nucl_elec_integral, (union_aux_mo_tot_num,union_aux_mo_tot_num)]
  implicit none
  BEGIN_DOC
  ! interaction nuclear electron on the MO basis
  END_DOC
  
  call union_aux_ao_to_union_aux_mo(                                 &
      union_aux_ao_nucl_elec_integral,                               &
      size(union_aux_ao_nucl_elec_integral,1),                       &
      union_aux_mo_nucl_elec_integral,                               &
      size(union_aux_mo_nucl_elec_integral,1)                        &
      )
  
END_PROVIDER


BEGIN_PROVIDER [double precision, union_aux_mo_nucl_elec_integral_per_atom, (union_aux_mo_tot_num,union_aux_mo_tot_num,nucl_num)]
  implicit none
  BEGIN_DOC
  ! union_aux_mo_nucl_elec_integral_per_atom(i,j,k) = -<MO(i)|1/|r-Rk|MO(j)>
  ! where Rk is the geometry of the kth atom
  END_DOC
  
  integer                        :: k
  union_aux_mo_nucl_elec_integral_per_atom = 0.d0
  do k = 1, nucl_num
    call union_aux_ao_to_union_aux_mo(                               &
        union_aux_ao_nucl_elec_integral_per_atom(1,1,k),             &
        size(union_aux_ao_nucl_elec_integral_per_atom,1),            &
        union_aux_mo_nucl_elec_integral_per_atom(1,1,k),             &
        size(union_aux_mo_nucl_elec_integral_per_atom,1)             &
        )
  enddo
  
END_PROVIDER

