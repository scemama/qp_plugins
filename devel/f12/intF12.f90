!*****************************************************************************
double precision function intF12S(ExpS,ExpA,CenterA,AngMomA,ExpB,CenterB,AngMomB,ExpC,CenterC,AngMomC,ExpD,CenterD,AngMomD) result(Gabcd)

! Compute two-electron integrals over Slater geminals
! via a fit using Gaussian geminals

  implicit none

! Input variables 

  double precision,intent(in)   :: ExpS
  double precision,intent(in)   :: ExpA,ExpB,ExpC,ExpD
  double precision,intent(in)   :: CenterA(3),CenterB(3),CenterC(3),CenterD(3)
  integer,intent(in)            :: AngMomA(3),AngMomB(3),AngMomC(3),AngMomD(3)


! Local variables

  double precision              :: ExpG(6),cG(6),ExpSG
  double precision              :: intF12G

  integer                       :: k

! Fitting parameters

  ExpG = (/ 0.2209d0, 1.004d0,  3.622d0, 12.16d0,   45.87d0,  254.4d0     /)
  cG   = (/ 0.3144d0, 0.3037d0, 0.1681d0, 0.09811d0, 0.06024d0, 0.03726d0 /)

  Gabcd = 0d0
  Do k=1,6
    ExpSG = ExpG(k)*ExpS**2d0
    Gabcd = Gabcd + cG(k)*intF12G(ExpSG,ExpA,CenterA,AngMomA,ExpB,CenterB,AngMomB, &
                                        ExpC,CenterC,AngMomC,ExpD,CenterD,AngMomD)
  Enddo

end function intF12S
!*****************************************************************************

!*****************************************************************************
double precision function intF12G(ExpG,ExpA,CenterA,AngMomA,ExpB,CenterB,AngMomB,ExpC,CenterC,AngMomC,ExpD,CenterD,AngMomD)

! Compute two-electron integrals over Gaussian geminals

  implicit none

! Input variables 

  double precision,intent(in)   :: ExpG
  double precision,intent(in)   :: ExpA,ExpB,ExpC,ExpD
  double precision,intent(in)   :: CenterA(3),CenterB(3),CenterC(3),CenterD(3)
  integer,intent(in)            :: AngMomA(3),AngMomB(3),AngMomC(3),AngMomD(3)


! Local variables

  double precision              :: ExpAi,ExpBi,ExpCi,ExpDi,ExpGi
  double precision              :: ExpP,ExpQ,ExpPi,ExpQi,ExpPGQi
  double precision              :: CenterP(3),CenterQ(3),CenterAB(3),CenterCD(3),CenterPQSq(3),CenterRA(3),CenterRC(3)
  double precision              :: NormABSq,NormCDSq
  double precision              :: GAB,GCD
  double precision              :: fP,fG,fQ,gP,gG,gQ
  double precision              :: GHRR

  integer                       :: i
  double precision              :: pi

! Output variables 
  double precision              :: Gabcd(3)

  pi = 4d0*atan(1d0)

! Pre-computed shell quantities

  ExpAi = 1d0/ExpA
  ExpBi = 1d0/ExpB
  ExpCi = 1d0/ExpC
  ExpDi = 1d0/ExpD
  ExpGi = 1d0/ExpG

! Pre-computed quantities for shell-pair AB

  ExpP  = ExpA + ExpB
  ExpPi = 1d0/ExpP

  NormABSq = 0d0
  Do i=1,3
    CenterP(i) = (ExpA*CenterA(i) + ExpB*CenterB(i))/ExpP
    CenterAB(i) = CenterA(i) - CenterB(i)
    NormABSq = NormABSq + CenterAB(i)**2d0
  Enddo

  GAB = (pi/ExpP)**(1.5d0)*exp(-NormABSq/(ExpAi+ExpBi))

! Pre-computed quantities for shell-pair CD

  ExpQ  = ExpC + ExpD
  ExpQi = 1d0/ExpQ

  NormCDSq = 0d0
  Do i=1,3
    CenterQ(i) = (ExpC*CenterC(i) + ExpD*CenterD(i))/ExpQ
    CenterCD(i) = CenterC(i) - CenterD(i)
    NormCDSq = NormCDSq + CenterCD(i)**2d0
  Enddo

  GCD = (pi/ExpQ)**(1.5d0)*exp(-NormCDSq/(ExpCi+ExpDi))

! Pre-computed shell-quartet quantities

  ExpPGQi = ExpPi + ExpGi + ExpQi

  Do i=1,3
    CenterPQSq(i) = (CenterP(i) - CenterQ(i))**2d0
  Enddo
  
  fP = ExpPi/ExpPGQi
  fG = ExpGi/ExpPGQi
  fQ = ExpQi/ExpPGQi

  gP = (1d0 - fP)/(2d0*ExpP)
  gG = fP/(2d0*expQ)
  gQ = (1d0 - fQ)/(2d0*ExpQ)

  do i=1,3
    CenterRA(i) = CenterP(i) - CenterA(i) + fP*(CenterQ(i) - CenterP(i))
    CenterRC(i) = CenterQ(i) - CenterC(i) + fQ*(CenterP(i) - CenterQ(i))
  enddo

! Loop over cartesian directions
  Do i=1,3
    Gabcd(i) = GHRR(AngMomA(i),AngMomB(i),AngMomC(i),AngMomD(i),fG,gP,gG,gQ,ExpPGQi, &
                 CenterPQSq(i),CenterRA(i),CenterRC(i),CenterAB(i),CenterCD(i))
  Enddo

  intF12G = GAB*GCD*Gabcd(1)*Gabcd(2)*Gabcd(3)

end function intF12G
!*****************************************************************************

!*****************************************************************************
recursive function GVRR(AngMomA,AngMomC,fG,gP,gG,gQ,ExpPGQi,CenterPQSq,CenterRA,CenterRC) &
                   result(Gac)

! Compute two-electron integrals over Gaussian geminals

  implicit none

! Input variables 

  integer,intent(in)            :: AngMomA,AngMomC
  double precision,intent(in)   :: ExpPGQi
  double precision,intent(in)   :: fG,gP,gG,gQ
  double precision,intent(in)   :: CenterPQSq,CenterRA,CenterRC

! Output variables

  double precision              :: Gac

  If(AngMomA < 0 .or. AngMomC < 0) then
    Gac = 0d0
  Else
    If(AngMomA == 0 .and. AngMomC == 0) then
      Gac = sqrt(fG)*exp(-CenterPQSq/ExpPGQi)
    Else
      If(AngMomC == 0) then
        Gac = CenterRA*GVRR(AngMomA-1,AngMomC,fG,gP,gG,gQ,ExpPGQi,CenterPQSq,CenterRA,CenterRC)           &
            + dble(AngMomA-1)*gP*GVRR(AngMomA-2,AngMomC,fG,gP,gG,gQ,ExpPGQi,CenterPQSq,CenterRA,CenterRC)
      Else
        Gac = CenterRC*GVRR(AngMomA,AngMomC-1,fG,gP,gG,gQ,ExpPGQi,CenterPQSq,CenterRA,CenterRC)           &
            + dble(AngMomA)*gG*GVRR(AngMomA-1,AngMomC-1,fG,gP,gG,gQ,ExpPGQi,CenterPQSq,CenterRA,CenterRC) &
            + dble(AngMomC-1)*gQ*GVRR(AngMomA,AngMomC-2,fG,gP,gG,gQ,ExpPGQi,CenterPQSq,CenterRA,CenterRC)
      EndIf
    EndIf
  EndIf
   
end function GVRR
!*****************************************************************************

!*****************************************************************************
recursive function GHRR(AngMomA,AngMomB,AngMomC,AngMomD,fG,gP,gG,gQ,ExpPGQi, &
                        CenterPQSq,CenterRA,CenterRC,CenterAB,CenterCD)      &
                   result(Gabcd)

! Compute two-electron integrals over Gaussian geminals

  implicit none

! Input variables 
  integer,intent(in)            :: AngMomA,AngMomB,AngMomC,AngMomD
  double precision,intent(in)   :: ExpPGQi
  double precision,intent(in)   :: fG,gP,gG,gQ
  double precision,intent(in)   :: CenterPQSq,CenterRA,CenterRC
  double precision,intent(in)   :: CenterAB,CenterCD

! Local variables
  double precision              :: GVRR
  double precision              :: Gabcd

  If(AngMomB < 0 .or. AngMomD < 0) then
    Gabcd = 0d0
  Else
    If(AngMomB == 0 .and. AngMomD == 0) then
      Gabcd = GVRR(AngMomA,AngMomC,fG,gP,gG,gQ,ExpPGQi,CenterPQSq,CenterRA,CenterRC)
    Else
      If(AngMomD == 0) then
        Gabcd = GHRR(AngMomA+1,AngMomB-1,AngMomC,AngMomD,fG,gP,gG,gQ,ExpPGQi, & 
                  CenterPQSq,CenterRA,CenterRC,CenterAB,CenterCD)             &
              + CenterAB*GHRR(AngMomA,AngMomB-1,AngMomC,AngMomD,fG,gP,gG,gQ,  &
                  ExpPGQi,CenterPQSq,CenterRA,CenterRC,CenterAB,CenterCD)
      Else
        Gabcd = GHRR(AngMomA,AngMomB,AngMomC+1,AngMomD-1,fG,gP,gG,gQ,ExpPGQi, & 
                  CenterPQSq,CenterRA,CenterRC,CenterAB,CenterCD)             &
              + CenterCD*GHRR(AngMomA,AngMomB,AngMomC,AngMomD-1,fG,gP,gG,gQ,  &
                  ExpPGQi,CenterPQSq,CenterRA,CenterRC,CenterAB,CenterCD)
      EndIf
    EndIf
  EndIf
   
end function GHRR
!*****************************************************************************
