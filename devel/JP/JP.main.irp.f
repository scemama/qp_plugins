program JP
  read_wf = .true.
  mrmode=3
  call run
end

subroutine run
  implicit none
  BEGIN_DOC
! TODO
  END_DOC

  integer :: i,j
  
  print *,  'CAS'
  do i=1,N_det_cas
    print *,  (psi_cas_coef(i,j), j=1,N_states)
    call debug_det(psi_cas(1,1,i),N_int)
  enddo 
  print *,  ''
  do i=1,N_det_ref
   print *,  H_cas(:,i)
  enddo

  double precision :: psi(2,2), psi2(2,2), E(2), R(2,2), H(2,2), pi, t, u, c
  pi = dacos(-1.d0)
  R(1,1) = cos(pi/4.)
  R(2,2) = cos(pi/4.)
  R(1,2) = sin(pi/4.)
  R(2,1) = -sin(pi/4.)
  H = matmul(transpose(R),H_cas)
  H_cas(1:2,1:2) = matmul(H,R)
  call lapack_diag(E,psi,H_cas(1:2,1:2),2,2)
  print *,  '----'
  print *,  'Psi = ', psi(:,1)
  print *,  'E = , ',  E(:)
  print *,  'Delta E = ', E(2)-E(1)
  print *,  '----'

  R(1,1) = nucl_dist(1,2)  * 0.529177249d0
  t = H_cas(1,2)
  u = H_cas(2,2)-H_cas(1,1)
  c = psi(2,1)/psi(1,1)
  E += nuclear_repulsion
  print '(A3,X,F6.2,X,7(F13.8,X))',  'JP ', R(1,1), E(1), E(2), H_cas(1,1)+nuclear_repulsion, &
  H_cas(2,2)+nuclear_repulsion, t, u, c


  ! Rotation de pi/4

!  double precision :: E, hij
!  E = 0.d0
!  do i=1,N_det_ref
!    do j=1,N_det_ref
!      call i_H_j(psi_ref(1,1,i), psi_ref(1,1,j), N_int, hij)
!      E += psi_ref_coef(i,1)*psi_ref_coef(j,1)*hij
!    enddo
!  enddo
!  do i=1,N_det_non_ref
!    do j=1,N_det_non_ref
!      call i_H_j(psi_non_ref(1,1,i), psi_non_ref(1,1,j), N_int, hij)
!      E += psi_non_ref_coef(i,1)*psi_non_ref_coef(j,1)*hij
!    enddo
!  enddo
!  do i=1,N_det
!    do j=1,N_det_non_ref
!      call i_H_j(psi_ref(1,1,i), psi_non_ref(1,1,j), N_int, hij)
!      E += 2.d0*psi_ref_coef(i,1)*psi_non_ref_coef(j,1)*hij
!    enddo
!  enddo
!  print *,  E + nuclear_repulsion

end
