BEGIN_PROVIDER [ double precision, H_band, (N_det_ref, N_det_non_ref) ]
 implicit none
 BEGIN_DOC
 ! 
 END_DOC
 integer :: i,j

  do i=1,N_det_ref
    do j=1,N_det_non_ref
        call i_H_j(psi_ref(1,1,i), psi_non_ref(1,1,j), N_int, H_band(i,j))
!        H_band(i,j) += delta_ij(1,j,i)
    enddo
  enddo
END_PROVIDER


BEGIN_PROVIDER [ double precision, H_cas, (N_det_ref, N_det_ref) ]
 implicit none
 BEGIN_DOC
 ! 
 END_DOC
 integer :: i,j,k
 double precision :: hij

  do i=1,N_det_ref
    do j=1,N_det_ref
      call i_H_j(psi_ref(1,1,i), psi_ref(1,1,j), N_int, H_cas(i,j))
      do k=1,N_det_non_ref
!        H_cas(i,j) += H_band(i,k) * H_band(j,k) * lambda_mrcc(1,k)
        H_cas(i,j) += H_band(i,k) * dij(j,k,1)
      enddo
    enddo
!    do k=1,N_det_non_ref
!      if (dij(i,k,1) == 0.d0) then
!        call i_H_j(psi_ref(1,1,i), psi_non_ref(1,1,k), N_int, hij)
!        H_cas(i,i) += hij*psi_non_ref_coef(k,1)/psi_ref_coef(i,1)
!        endif
!    enddo
!    H_cas(i,i) += delta_ii(1,i)
  enddo

END_PROVIDER


