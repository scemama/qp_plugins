use map_module

!! AO Map
!! ======

BEGIN_PROVIDER [ type(map_type), union_aux_ao_integrals_map ]
  implicit none
  BEGIN_DOC
  ! AO integrals
  END_DOC
  integer(key_kind)              :: key_max
  integer(map_size_kind)         :: sze
  call union_aux_bielec_integrals_index(union_aux_ao_num,union_aux_ao_num,union_aux_ao_num,union_aux_ao_num,key_max)
  sze = key_max
  call map_init(union_aux_ao_integrals_map,sze)
  print*,  'AO map initialized : ', sze
END_PROVIDER

subroutine union_aux_bielec_integrals_index(i,j,k,l,i1)
  use map_module
  implicit none
  integer, intent(in)            :: i,j,k,l
  integer(key_kind), intent(out) :: i1
  integer(key_kind)              :: p,q,r,s,i2
  p = min(i,k)
  r = max(i,k)
  p = p+ishft(r*r-r,-1)
  q = min(j,l)
  s = max(j,l)
  q = q+ishft(s*s-s,-1)
  i1 = min(p,q)
  i2 = max(p,q)
  i1 = i1+ishft(i2*i2-i2,-1)
end

subroutine bielec_integrals_index_reverse(i,j,k,l,i1)
  use map_module
  implicit none
  integer, intent(out)           :: i(8),j(8),k(8),l(8)
  integer(key_kind), intent(in)  :: i1
  integer(key_kind)              :: i2,i3
  i = 0
  i2   = ceiling(0.5d0*(dsqrt(8.d0*dble(i1)+1.d0)-1.d0))
  l(1) = ceiling(0.5d0*(dsqrt(8.d0*dble(i2)+1.d0)-1.d0))
  i3   = i1 - ishft(i2*i2-i2,-1)
  k(1) = ceiling(0.5d0*(dsqrt(8.d0*dble(i3)+1.d0)-1.d0))
  j(1) = int(i2 - ishft(l(1)*l(1)-l(1),-1),4)
  i(1) = int(i3 - ishft(k(1)*k(1)-k(1),-1),4)

              !ijkl
  i(2) = i(1) !ilkj
  j(2) = l(1)
  k(2) = k(1)
  l(2) = j(1)

  i(3) = k(1) !kjil
  j(3) = j(1)
  k(3) = i(1)
  l(3) = l(1)

  i(4) = k(1) !klij
  j(4) = l(1)
  k(4) = i(1)
  l(4) = j(1)

  i(5) = j(1) !jilk
  j(5) = i(1)
  k(5) = l(1)
  l(5) = k(1)

  i(6) = j(1) !jkli
  j(6) = k(1)
  k(6) = l(1)
  l(6) = i(1)

  i(7) = l(1) !lijk
  j(7) = i(1)
  k(7) = j(1)
  l(7) = k(1)

  i(8) = l(1) !lkji
  j(8) = k(1)
  k(8) = j(1)
  l(8) = i(1)

  integer :: ii, jj
  do ii=2,8
    do jj=1,ii-1
      if ( (i(ii) == i(jj)).and. &
           (j(ii) == j(jj)).and. &
           (k(ii) == k(jj)).and. &
           (l(ii) == l(jj)) ) then
         i(ii) = 0
         exit
      endif
    enddo
  enddo
  do ii=1,8
    if (i(ii) /= 0) then
      call union_aux_bielec_integrals_index(i(ii),j(ii),k(ii),l(ii),i2)
      if (i1 /= i2) then
        print *,  i1, i2
        print *,  i(ii), j(jj), k(jj), l(jj)
        stop 'bielec_integrals_index_reverse failed'
      endif
    endif
  enddo


end

 BEGIN_PROVIDER [ integer, union_aux_ao_integrals_cache_min ]
&BEGIN_PROVIDER [ integer, union_aux_ao_integrals_cache_max ]
 implicit none
 BEGIN_DOC
 ! Min and max values of the AOs for which the integrals are in the cache
 END_DOC
 union_aux_ao_integrals_cache_min = max(1,union_aux_ao_num - 63)
 union_aux_ao_integrals_cache_max = union_aux_ao_num

END_PROVIDER

BEGIN_PROVIDER [ double precision, union_aux_ao_integrals_cache, (0:64*64*64*64) ]
 implicit none
 BEGIN_DOC
 ! Cache of AO integrals for fast access
 END_DOC
 PROVIDE union_aux_ao_bielec_integrals_in_map
 integer                        :: i,j,k,l,ii
 integer(key_kind)              :: idx
 real(integral_kind)            :: integral
 !$OMP PARALLEL DO PRIVATE (i,j,k,l,idx,ii,integral)
 do l=union_aux_ao_integrals_cache_min,union_aux_ao_integrals_cache_max
   do k=union_aux_ao_integrals_cache_min,union_aux_ao_integrals_cache_max
     do j=union_aux_ao_integrals_cache_min,union_aux_ao_integrals_cache_max
       do i=union_aux_ao_integrals_cache_min,union_aux_ao_integrals_cache_max
         !DIR$ FORCEINLINE
         call union_aux_bielec_integrals_index(i,j,k,l,idx)
         !DIR$ FORCEINLINE
         call map_get(union_aux_ao_integrals_map,idx,integral)
         ii = l-union_aux_ao_integrals_cache_min
         ii = ior( ishft(ii,6), k-union_aux_ao_integrals_cache_min)
         ii = ior( ishft(ii,6), j-union_aux_ao_integrals_cache_min)
         ii = ior( ishft(ii,6), i-union_aux_ao_integrals_cache_min)
         union_aux_ao_integrals_cache(ii) = integral
       enddo
     enddo
   enddo
 enddo
 !$OMP END PARALLEL DO

END_PROVIDER


double precision function get_union_aux_ao_bielec_integral(i,j,k,l,map) result(result)
  use map_module
  implicit none
  BEGIN_DOC
  ! Gets one AO bi-electronic integral from the AO map
  END_DOC
  integer, intent(in)            :: i,j,k,l
  integer(key_kind)              :: idx
  type(map_type), intent(inout)  :: map
  integer                        :: ii
  real(integral_kind)            :: tmp
  PROVIDE union_aux_ao_bielec_integrals_in_map union_aux_ao_integrals_cache union_aux_ao_integrals_cache_min
  !DIR$ FORCEINLINE
  if (union_aux_ao_overlap_abs(i,k)*union_aux_ao_overlap_abs(j,l) < union_aux_ao_integrals_threshold ) then
    tmp = 0.d0
  else if (union_aux_ao_bielec_integral_schwartz(i,k)*union_aux_ao_bielec_integral_schwartz(j,l) < union_aux_ao_integrals_threshold) then
    tmp = 0.d0
  else
    ii = l-union_aux_ao_integrals_cache_min
    ii = ior(ii, k-union_aux_ao_integrals_cache_min)
    ii = ior(ii, j-union_aux_ao_integrals_cache_min)
    ii = ior(ii, i-union_aux_ao_integrals_cache_min)
    if (iand(ii, -64) /= 0) then
      !DIR$ FORCEINLINE
      call union_aux_bielec_integrals_index(i,j,k,l,idx)
      !DIR$ FORCEINLINE
      call map_get(map,idx,tmp)
    else
      ii = l-union_aux_ao_integrals_cache_min
      ii = ior( ishft(ii,6), k-union_aux_ao_integrals_cache_min)
      ii = ior( ishft(ii,6), j-union_aux_ao_integrals_cache_min)
      ii = ior( ishft(ii,6), i-union_aux_ao_integrals_cache_min)
      tmp = union_aux_ao_integrals_cache(ii)
    endif
  endif
  result = tmp
end


subroutine get_union_aux_ao_bielec_integrals(j,k,l,sze,out_val)
  use map_module
  BEGIN_DOC
  ! Gets multiple AO bi-electronic integral from the AO map .
  ! All i are retrieved for j,k,l fixed.
  END_DOC
  implicit none
  integer, intent(in)            :: j,k,l, sze
  real(integral_kind), intent(out) :: out_val(sze)
  
  integer                        :: i
  integer(key_kind)              :: hash
  double precision               :: thresh
  PROVIDE union_aux_ao_bielec_integrals_in_map union_aux_ao_integrals_map
  thresh = union_aux_ao_integrals_threshold
  
  if (union_aux_ao_overlap_abs(j,l) < thresh) then
    out_val = 0.d0
    return
  endif
  
  double precision :: get_union_aux_ao_bielec_integral
  do i=1,sze
    out_val(i) = get_union_aux_ao_bielec_integral(i,j,k,l,union_aux_ao_integrals_map)
  enddo
  
end

subroutine get_union_aux_ao_bielec_integrals_non_zero(j,k,l,sze,out_val,out_val_index,non_zero_int)
  use map_module
  implicit none
  BEGIN_DOC
  ! Gets multiple AO bi-electronic integral from the AO map .
  ! All non-zero i are retrieved for j,k,l fixed.
  END_DOC
  integer, intent(in)            :: j,k,l, sze
  real(integral_kind), intent(out) :: out_val(sze)
  integer, intent(out)           :: out_val_index(sze),non_zero_int
  
  integer                        :: i
  integer(key_kind)              :: hash
  double precision               :: thresh,tmp
  PROVIDE union_aux_ao_bielec_integrals_in_map
  thresh = union_aux_ao_integrals_threshold
  
  non_zero_int = 0
  if (union_aux_ao_overlap_abs(j,l) < thresh) then
    out_val = 0.d0
    return
  endif
 
  non_zero_int = 0
  do i=1,sze
    integer, external :: union_aux_ao_l4
    double precision, external :: union_aux_ao_bielec_integral
    !DIR$ FORCEINLINE
    if (union_aux_ao_bielec_integral_schwartz(i,k)*union_aux_ao_bielec_integral_schwartz(j,l) < thresh) then
      cycle
    endif
    call union_aux_bielec_integrals_index(i,j,k,l,hash)
    call map_get(union_aux_ao_integrals_map, hash,tmp)
    if (dabs(tmp) < thresh ) cycle
    non_zero_int = non_zero_int+1
    out_val_index(non_zero_int) = i
    out_val(non_zero_int) = tmp
  enddo
  
end


function get_union_aux_ao_map_size()
  implicit none
  integer (map_size_kind) :: get_union_aux_ao_map_size
  BEGIN_DOC
  ! Returns the number of elements in the AO map
  END_DOC
  get_union_aux_ao_map_size = union_aux_ao_integrals_map % n_elements
end

subroutine clear_union_aux_ao_map
  implicit none
  BEGIN_DOC
  ! Frees the memory of the AO map
  END_DOC
  call map_deinit(union_aux_ao_integrals_map)
  FREE union_aux_ao_integrals_map
end


!! MO Map
!! ======

BEGIN_PROVIDER [ type(map_type), union_aux_mo_integrals_map ]
  implicit none
  BEGIN_DOC
  ! MO integrals
  END_DOC
  integer(key_kind)              :: key_max
  integer(map_size_kind)         :: sze
  call union_aux_bielec_integrals_index(union_aux_mo_tot_num,union_aux_mo_tot_num,union_aux_mo_tot_num,union_aux_mo_tot_num,key_max)
  sze = key_max
  call map_init(union_aux_mo_integrals_map,sze)
  print*, 'MO map initialized: ', sze
END_PROVIDER

subroutine insert_into_union_aux_ao_integrals_map(n_integrals,buffer_i, buffer_values)
  use map_module
  implicit none
  BEGIN_DOC
  ! Create new entry into AO map
  END_DOC
  
  integer, intent(in)                :: n_integrals
  integer(key_kind), intent(inout)   :: buffer_i(n_integrals)
  real(integral_kind), intent(inout) :: buffer_values(n_integrals)
  
  call map_append(union_aux_ao_integrals_map, buffer_i, buffer_values, n_integrals)
end

subroutine insert_into_union_aux_mo_integrals_map(n_integrals,                 &
      buffer_i, buffer_values, thr)
  use map_module
  implicit none
  
  BEGIN_DOC
  ! Create new entry into MO map, or accumulate in an existing entry
  END_DOC
  
  integer, intent(in)                :: n_integrals
  integer(key_kind), intent(inout)   :: buffer_i(n_integrals)
  real(integral_kind), intent(inout) :: buffer_values(n_integrals)
  real(integral_kind), intent(in)    :: thr
  call map_update(union_aux_mo_integrals_map, buffer_i, buffer_values, n_integrals, thr)
end

 BEGIN_PROVIDER [ integer*4, union_aux_mo_integrals_cache_min ]
&BEGIN_PROVIDER [ integer*4, union_aux_mo_integrals_cache_max ]
&BEGIN_PROVIDER [ integer*8, union_aux_mo_integrals_cache_min_8 ]
&BEGIN_PROVIDER [ integer*8, union_aux_mo_integrals_cache_max_8 ]
 implicit none
 BEGIN_DOC
 ! Min and max values of the MOs for which the integrals are in the cache
 END_DOC
 union_aux_mo_integrals_cache_min_8 = max(1_8,elec_alpha_num - 63_8)
 union_aux_mo_integrals_cache_max_8 = min(int(union_aux_mo_tot_num,8),union_aux_mo_integrals_cache_min_8+127_8)
 union_aux_mo_integrals_cache_min   = max(1,elec_alpha_num - 63)
 union_aux_mo_integrals_cache_max   = min(union_aux_mo_tot_num,union_aux_mo_integrals_cache_min+127)

END_PROVIDER

BEGIN_PROVIDER [ double precision, union_aux_mo_integrals_cache, (0_8:128_8*128_8*128_8*128_8) ]
 implicit none
 BEGIN_DOC
 ! Cache of MO integrals for fast access
 END_DOC
 PROVIDE union_aux_mo_bielec_integrals_in_map
 integer*8                      :: i,j,k,l
 integer*4                      :: i4,j4,k4,l4
 integer*8                      :: ii
 integer(key_kind)              :: idx
 real(integral_kind)            :: integral
 FREE union_aux_ao_integrals_cache
 !$OMP PARALLEL DO PRIVATE (i,j,k,l,i4,j4,k4,l4,idx,ii,integral)
 do l=union_aux_mo_integrals_cache_min_8,union_aux_mo_integrals_cache_max_8
   l4 = int(l,4)
   do k=union_aux_mo_integrals_cache_min_8,union_aux_mo_integrals_cache_max_8
     k4 = int(k,4)
     do j=union_aux_mo_integrals_cache_min_8,union_aux_mo_integrals_cache_max_8
       j4 = int(j,4)
       do i=union_aux_mo_integrals_cache_min_8,union_aux_mo_integrals_cache_max_8
         i4 = int(i,4)
         !DIR$ FORCEINLINE
         call union_aux_bielec_integrals_index(i4,j4,k4,l4,idx)
         !DIR$ FORCEINLINE
         call map_get(union_aux_mo_integrals_map,idx,integral)
         ii = l-union_aux_mo_integrals_cache_min_8
         ii = ior( ishft(ii,7), k-union_aux_mo_integrals_cache_min_8)
         ii = ior( ishft(ii,7), j-union_aux_mo_integrals_cache_min_8)
         ii = ior( ishft(ii,7), i-union_aux_mo_integrals_cache_min_8)
         union_aux_mo_integrals_cache(ii) = integral
       enddo
     enddo
   enddo
 enddo
 !$OMP END PARALLEL DO

END_PROVIDER


double precision function get_union_aux_mo_bielec_integral(i,j,k,l,map)
  use map_module
  implicit none
  BEGIN_DOC
  ! Returns one integral <ij|kl> in the MO basis
  END_DOC
  integer, intent(in)            :: i,j,k,l
  integer(key_kind)              :: idx
  integer                        :: ii
  integer*8                      :: ii_8
  type(map_type), intent(inout)  :: map
  real(integral_kind)            :: tmp
  PROVIDE union_aux_mo_bielec_integrals_in_map union_aux_mo_integrals_cache
  ii = l-union_aux_mo_integrals_cache_min
  ii = ior(ii, k-union_aux_mo_integrals_cache_min)
  ii = ior(ii, j-union_aux_mo_integrals_cache_min)
  ii = ior(ii, i-union_aux_mo_integrals_cache_min)
  if (iand(ii, -128) /= 0) then
    !DIR$ FORCEINLINE
    call union_aux_bielec_integrals_index(i,j,k,l,idx)
    !DIR$ FORCEINLINE
    call map_get(map,idx,tmp)
    get_union_aux_mo_bielec_integral = dble(tmp)
  else
    ii_8 = int(l,8)-union_aux_mo_integrals_cache_min_8
    ii_8 = ior( ishft(ii_8,7), int(k,8)-union_aux_mo_integrals_cache_min_8)
    ii_8 = ior( ishft(ii_8,7), int(j,8)-union_aux_mo_integrals_cache_min_8)
    ii_8 = ior( ishft(ii_8,7), int(i,8)-union_aux_mo_integrals_cache_min_8)
    get_union_aux_mo_bielec_integral = union_aux_mo_integrals_cache(ii_8)
  endif
end


double precision function union_aux_mo_bielec_integral(i,j,k,l)
  implicit none
  BEGIN_DOC
  ! Returns one integral <ij|kl> in the MO basis
  END_DOC
  integer, intent(in)            :: i,j,k,l
  double precision               :: get_union_aux_mo_bielec_integral
  PROVIDE union_aux_mo_bielec_integrals_in_map union_aux_mo_integrals_cache
  !DIR$ FORCEINLINE
  PROVIDE union_aux_mo_bielec_integrals_in_map
  union_aux_mo_bielec_integral = get_union_aux_mo_bielec_integral(i,j,k,l,union_aux_mo_integrals_map)
  return
end

subroutine get_union_aux_mo_bielec_integrals(j,k,l,sze,out_val,map)
  use map_module
  implicit none
  BEGIN_DOC
  ! Returns multiple integrals <ij|kl> in the MO basis, all
  ! i for j,k,l fixed.
  END_DOC
  integer, intent(in)            :: j,k,l, sze
  double precision, intent(out)  :: out_val(sze)
  type(map_type), intent(inout)  :: map
  integer                        :: i
  integer(key_kind)              :: hash(sze)
  real(integral_kind)            :: tmp_val(sze)
  PROVIDE union_aux_mo_bielec_integrals_in_map
  
  do i=1,sze
    !DIR$ FORCEINLINE
    call union_aux_bielec_integrals_index(i,j,k,l,hash(i))
  enddo
  
  if (key_kind == 8) then
    call map_get_many(map, hash, out_val, sze)
  else
    call map_get_many(map, hash, tmp_val, sze)
    ! Conversion to double precision 
    do i=1,sze
      out_val(i) = dble(tmp_val(i))
    enddo
  endif
end

subroutine get_union_aux_mo_bielec_integrals_ij(k,l,sze,out_array,map)
  use map_module
  implicit none
  BEGIN_DOC
  ! Returns multiple integrals <ij|kl> in the MO basis, all
  ! i(1)j(2) 1/r12 k(1)l(2)
  ! i, j for k,l fixed.
  END_DOC
  integer, intent(in)            :: k,l, sze
  double precision, intent(out)  :: out_array(sze,sze)
  type(map_type), intent(inout)  :: map
  integer                        :: i,j,kk,ll,m
  integer(key_kind),allocatable  :: hash(:)
  integer  ,allocatable          :: pairs(:,:), iorder(:)
  real(integral_kind), allocatable :: tmp_val(:)

  PROVIDE union_aux_mo_bielec_integrals_in_map
  allocate (hash(sze*sze), pairs(2,sze*sze),iorder(sze*sze), &
  tmp_val(sze*sze))
  
  kk=0
  out_array = 0.d0
  do j=1,sze
   do i=1,sze
    kk += 1
    !DIR$ FORCEINLINE
    call union_aux_bielec_integrals_index(i,j,k,l,hash(kk))
    pairs(1,kk) = i
    pairs(2,kk) = j
    iorder(kk) = kk
   enddo
  enddo

  logical :: integral_is_in_map
  if (key_kind == 8) then
    call i8radix_sort(hash,iorder,kk,-1)
  else if (key_kind == 4) then
    call iradix_sort(hash,iorder,kk,-1)
  else if (key_kind == 2) then
    call i2radix_sort(hash,iorder,kk,-1)
  endif

  call map_get_many(union_aux_mo_integrals_map, hash, tmp_val, kk)

  do ll=1,kk
    m = iorder(ll)
    i=pairs(1,m)
    j=pairs(2,m)
    out_array(i,j) = tmp_val(ll)
  enddo  

  deallocate(pairs,hash,iorder,tmp_val)
end

subroutine get_union_aux_mo_bielec_integrals_coulomb_ii(k,l,sze,out_val,map)
  use map_module
  implicit none
  BEGIN_DOC
  ! Returns multiple integrals <ki|li> 
  ! k(1)i(2) 1/r12 l(1)i(2) :: out_val(i1)
  ! for k,l fixed.
  END_DOC
  integer, intent(in)            :: k,l, sze
  double precision, intent(out)  :: out_val(sze)
  type(map_type), intent(inout)  :: map
  integer                        :: i
  integer(key_kind)              :: hash(sze)
  real(integral_kind)            :: tmp_val(sze)
  PROVIDE union_aux_mo_bielec_integrals_in_map
  
  integer :: kk
  do i=1,sze
    !DIR$ FORCEINLINE
    call union_aux_bielec_integrals_index(k,i,l,i,hash(i))
  enddo
  
  if (key_kind == 8) then
    call map_get_many(map, hash, out_val, sze)
  else
    call map_get_many(map, hash, tmp_val, sze)
    ! Conversion to double precision 
    do i=1,sze
      out_val(i) = dble(tmp_val(i))
    enddo
  endif
end

subroutine get_union_aux_mo_bielec_integrals_exch_ii(k,l,sze,out_val,map)
  use map_module
  implicit none
  BEGIN_DOC
  ! Returns multiple integrals <ki|il> 
  ! k(1)i(2) 1/r12 i(1)l(2) :: out_val(i1)
  ! for k,l fixed.
  END_DOC
  integer, intent(in)            :: k,l, sze
  double precision, intent(out)  :: out_val(sze)
  type(map_type), intent(inout)  :: map
  integer                        :: i
  integer(key_kind)              :: hash(sze)
  real(integral_kind)            :: tmp_val(sze)
  PROVIDE union_aux_mo_bielec_integrals_in_map
  
  integer :: kk
  do i=1,sze
    !DIR$ FORCEINLINE
    call union_aux_bielec_integrals_index(k,i,i,l,hash(i))
  enddo
  
  if (key_kind == 8) then
    call map_get_many(map, hash, out_val, sze)
  else
    call map_get_many(map, hash, tmp_val, sze)
    ! Conversion to double precision 
    do i=1,sze
      out_val(i) = dble(tmp_val(i))
    enddo
  endif
end


integer*8 function get_union_aux_mo_map_size()
  implicit none
  BEGIN_DOC
  ! Return the number of elements in the MO map
  END_DOC
  get_union_aux_mo_map_size = union_aux_mo_integrals_map % n_elements
end


BEGIN_PROVIDER [ double precision, union_aux_ao_bielec_integral_schwartz,(union_aux_ao_num,union_aux_ao_num)  ]
  implicit none
  BEGIN_DOC
  !  Needed to compute Schwartz inequalities
  END_DOC
  
  integer                        :: i,k
  double precision               :: union_aux_ao_bielec_integral,cpu_1,cpu_2, wall_1, wall_2
  
  union_aux_ao_bielec_integral_schwartz(1,1) = union_aux_ao_bielec_integral(1,1,1,1)
  !$OMP PARALLEL DO PRIVATE(i,k)                                     &
      !$OMP DEFAULT(NONE)                                            &
      !$OMP SHARED (union_aux_ao_num,union_aux_ao_bielec_integral_schwartz)              &
      !$OMP SCHEDULE(dynamic)
  do i=1,union_aux_ao_num
    do k=1,i
      union_aux_ao_bielec_integral_schwartz(i,k) = dsqrt(union_aux_ao_bielec_integral(i,k,i,k))
      union_aux_ao_bielec_integral_schwartz(k,i) = union_aux_ao_bielec_integral_schwartz(i,k)
    enddo
  enddo
  !$OMP END PARALLEL DO
  
END_PROVIDER


double precision function general_primitive_integral(dim,            &
      P_new,P_center,fact_p,p,p_inv,iorder_p,                        &
      Q_new,Q_center,fact_q,q,q_inv,iorder_q)
  implicit none
  BEGIN_DOC
  ! Computes the integral <pq|rs> where p,q,r,s are Gaussian primitives
  END_DOC
  integer,intent(in)             :: dim
  include 'Utils/constants.include.F'
  double precision, intent(in)   :: P_new(0:max_dim,3),P_center(3),fact_p,p,p_inv
  double precision, intent(in)   :: Q_new(0:max_dim,3),Q_center(3),fact_q,q,q_inv
  integer, intent(in)            :: iorder_p(3)
  integer, intent(in)            :: iorder_q(3)
  
  double precision               :: r_cut,gama_r_cut,rho,dist
  double precision               :: dx(0:max_dim),Ix_pol(0:max_dim),dy(0:max_dim),Iy_pol(0:max_dim),dz(0:max_dim),Iz_pol(0:max_dim)
  integer                        :: n_Ix,n_Iy,n_Iz,nx,ny,nz
  double precision               :: bla
  integer                        :: ix,iy,iz,jx,jy,jz,i
  double precision               :: a,b,c,d,e,f,accu,pq,const
  double precision               :: pq_inv, p10_1, p10_2, p01_1, p01_2,pq_inv_2
  integer                        :: n_pt_tmp,n_pt_out, iorder
  double precision               :: d1(0:max_dim),d_poly(0:max_dim),rint,d1_screened(0:max_dim)
  
  general_primitive_integral = 0.d0
  
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: dx,Ix_pol,dy,Iy_pol,dz,Iz_pol
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: d1, d_poly
  
  ! Gaussian Product
  ! ----------------
  
  pq = p_inv*0.5d0*q_inv
  pq_inv = 0.5d0/(p+q)
  p10_1 = q*pq  ! 1/(2p)
  p01_1 = p*pq  ! 1/(2q)
  pq_inv_2 = pq_inv+pq_inv
  p10_2 = pq_inv_2 * p10_1*q !0.5d0*q/(pq + p*p)
  p01_2 = pq_inv_2 * p01_1*p !0.5d0*p/(q*q + pq)
  
  
  accu = 0.d0
  iorder = iorder_p(1)+iorder_q(1)+iorder_p(1)+iorder_q(1)
  do ix=0,iorder
    Ix_pol(ix) = 0.d0
  enddo
  n_Ix = 0
  do ix = 0, iorder_p(1)
    if (abs(P_new(ix,1)) < thresh) cycle
    a = P_new(ix,1)
    do jx = 0, iorder_q(1)
      d = a*Q_new(jx,1)
      if (abs(d) < thresh) cycle
      !DIR$ FORCEINLINE
      call give_polynom_mult_center_x(P_center(1),Q_center(1),ix,jx,p,q,iorder,pq_inv,pq_inv_2,p10_1,p01_1,p10_2,p01_2,dx,nx)
      !DIR$ FORCEINLINE
      call add_poly_multiply(dx,nx,d,Ix_pol,n_Ix)
    enddo
  enddo
  if (n_Ix == -1) then
    return
  endif
  iorder = iorder_p(2)+iorder_q(2)+iorder_p(2)+iorder_q(2)
  do ix=0, iorder
    Iy_pol(ix) = 0.d0
  enddo
  n_Iy = 0
  do iy = 0, iorder_p(2)
    if (abs(P_new(iy,2)) > thresh) then
      b = P_new(iy,2)
      do jy = 0, iorder_q(2)
        e = b*Q_new(jy,2)
        if (abs(e) < thresh) cycle
        !DIR$ FORCEINLINE
        call   give_polynom_mult_center_x(P_center(2),Q_center(2),iy,jy,p,q,iorder,pq_inv,pq_inv_2,p10_1,p01_1,p10_2,p01_2,dy,ny)
        !DIR$ FORCEINLINE
        call add_poly_multiply(dy,ny,e,Iy_pol,n_Iy)
      enddo
    endif
  enddo
  if (n_Iy == -1) then
    return
  endif
  
  iorder = iorder_p(3)+iorder_q(3)+iorder_p(3)+iorder_q(3)
  do ix=0,iorder
    Iz_pol(ix) = 0.d0
  enddo
  n_Iz = 0
  do iz = 0, iorder_p(3)
    if (abs(P_new(iz,3)) > thresh) then
      c = P_new(iz,3)
      do jz = 0, iorder_q(3)
        f = c*Q_new(jz,3)
        if (abs(f) < thresh) cycle
        !DIR$ FORCEINLINE
        call   give_polynom_mult_center_x(P_center(3),Q_center(3),iz,jz,p,q,iorder,pq_inv,pq_inv_2,p10_1,p01_1,p10_2,p01_2,dz,nz)
        !DIR$ FORCEINLINE
        call add_poly_multiply(dz,nz,f,Iz_pol,n_Iz)
      enddo
    endif
  enddo
  if (n_Iz == -1) then
    return
  endif
  
  rho = p*q *pq_inv_2
  dist =  (P_center(1) - Q_center(1))*(P_center(1) - Q_center(1)) +  &
      (P_center(2) - Q_center(2))*(P_center(2) - Q_center(2)) +      &
      (P_center(3) - Q_center(3))*(P_center(3) - Q_center(3))
  const = dist*rho
  
  n_pt_tmp = n_Ix+n_Iy
  do i=0,n_pt_tmp
    d_poly(i)=0.d0
  enddo
  
  !DIR$ FORCEINLINE
  call multiply_poly(Ix_pol,n_Ix,Iy_pol,n_Iy,d_poly,n_pt_tmp)
  if (n_pt_tmp == -1) then
    return
  endif
  n_pt_out = n_pt_tmp+n_Iz
  do i=0,n_pt_out
    d1(i)=0.d0
  enddo
  
  !DIR$ FORCEINLINE
  call multiply_poly(d_poly ,n_pt_tmp ,Iz_pol,n_Iz,d1,n_pt_out)
  double precision               :: rint_sum
  accu = accu + rint_sum(n_pt_out,const,d1)
  
  general_primitive_integral = fact_p * fact_q * accu *pi_5_2*p_inv*q_inv/dsqrt(p+q)
end


double precision function ERI(alpha,beta,delta,gama,a_x,b_x,c_x,d_x,a_y,b_y,c_y,d_y,a_z,b_z,c_z,d_z)
  implicit none
  BEGIN_DOC
  !  ATOMIC PRIMTIVE bielectronic integral between the 4 primitives :: 
  !         primitive_1 = x1**(a_x) y1**(a_y) z1**(a_z) exp(-alpha * r1**2)
  !         primitive_2 = x1**(b_x) y1**(b_y) z1**(b_z) exp(- beta * r1**2)
  !         primitive_3 = x2**(c_x) y2**(c_y) z2**(c_z) exp(-delta * r2**2)
  !         primitive_4 = x2**(d_x) y2**(d_y) z2**(d_z) exp(- gama * r2**2)
  END_DOC
  double precision, intent(in)   :: delta,gama,alpha,beta
  integer, intent(in)            :: a_x,b_x,c_x,d_x,a_y,b_y,c_y,d_y,a_z,b_z,c_z,d_z
  integer                        :: a_x_2,b_x_2,c_x_2,d_x_2,a_y_2,b_y_2,c_y_2,d_y_2,a_z_2,b_z_2,c_z_2,d_z_2
  integer                        :: i,j,k,l,n_pt
  integer                        :: n_pt_sup
  double precision               :: p,q,denom,coeff
  double precision               :: I_f
  integer                        :: nx,ny,nz
  include 'Utils/constants.include.F'
  nx = a_x+b_x+c_x+d_x
  if(iand(nx,1) == 1) then
    ERI = 0.d0
    return
  endif

  ny = a_y+b_y+c_y+d_y
  if(iand(ny,1) == 1) then
    ERI = 0.d0
    return
  endif

  nz = a_z+b_z+c_z+d_z
  if(iand(nz,1) == 1) then
    ERI = 0.d0
    return
  endif
    
  ASSERT (alpha >= 0.d0)
  ASSERT (beta >= 0.d0)
  ASSERT (delta >= 0.d0)
  ASSERT (gama >= 0.d0)
  p = alpha + beta
  q = delta + gama
  ASSERT (p+q >= 0.d0)
  n_pt =  ishft( nx+ny+nz,1 )
  
  coeff = pi_5_2 / (p * q * dsqrt(p+q))
  if (n_pt == 0) then
    ERI = coeff
    return
  endif
  
  call integrale_new(I_f,a_x,b_x,c_x,d_x,a_y,b_y,c_y,d_y,a_z,b_z,c_z,d_z,p,q,n_pt)
  
  ERI = I_f * coeff
end


subroutine integrale_new(I_f,a_x,b_x,c_x,d_x,a_y,b_y,c_y,d_y,a_z,b_z,c_z,d_z,p,q,n_pt)
  BEGIN_DOC
  ! calculate the integral of the polynom :: 
  !         I_x1(a_x+b_x, c_x+d_x,p,q) * I_x1(a_y+b_y, c_y+d_y,p,q) * I_x1(a_z+b_z, c_z+d_z,p,q)
  ! between ( 0 ; 1)
  END_DOC
  
  
  implicit none
  include 'Utils/constants.include.F'
  double precision               :: p,q
  integer                        :: a_x,b_x,c_x,d_x,a_y,b_y,c_y,d_y,a_z,b_z,c_z,d_z
  integer                        :: i, n_iter, n_pt, j
  double precision               :: I_f, pq_inv, p10_1, p10_2, p01_1, p01_2,rho,pq_inv_2
  integer :: ix,iy,iz, jx,jy,jz, sx,sy,sz
  
  j = ishft(n_pt,-1)
  ASSERT (n_pt > 1)
  pq_inv = 0.5d0/(p+q)
  pq_inv_2 = pq_inv + pq_inv
  p10_1 = 0.5d0/p
  p01_1 = 0.5d0/q
  p10_2 = 0.5d0 *  q /(p * q + p * p)
  p01_2 = 0.5d0 *  p /(q * q + q * p)
  double precision               :: B00(n_pt_max_integrals)
  double precision               :: B10(n_pt_max_integrals), B01(n_pt_max_integrals)
  double precision               :: t1(n_pt_max_integrals), t2(n_pt_max_integrals)
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: t1, t2, B10, B01, B00
  ix = a_x+b_x
  jx = c_x+d_x
  iy = a_y+b_y
  jy = c_y+d_y
  iz = a_z+b_z
  jz = c_z+d_z
  sx = ix+jx
  sy = iy+jy
  sz = iz+jz

  do i = 1,n_pt
    B10(i)  = p10_1 -  gauleg_t2(i,j)* p10_2
    B01(i)  = p01_1 -  gauleg_t2(i,j)* p01_2
    B00(i)  = gauleg_t2(i,j)*pq_inv
  enddo
  if (sx > 0) then
    call I_x1_new(ix,jx,B10,B01,B00,t1,n_pt)
  else
    do i = 1,n_pt
      t1(i) = 1.d0
    enddo
  endif
  if (sy > 0) then
    call I_x1_new(iy,jy,B10,B01,B00,t2,n_pt)
    do i = 1,n_pt
      t1(i) = t1(i)*t2(i)
    enddo
  endif
  if (sz > 0) then
    call I_x1_new(iz,jz,B10,B01,B00,t2,n_pt)
    do i = 1,n_pt
      t1(i) = t1(i)*t2(i)
    enddo
  endif
  I_f= 0.d0
  do i = 1,n_pt
    I_f += gauleg_w(i,j)*t1(i)
  enddo
  
  
  
end

recursive subroutine I_x1_new(a,c,B_10,B_01,B_00,res,n_pt) 
  BEGIN_DOC
  !  recursive function involved in the bielectronic integral
  END_DOC
  implicit none
  include 'Utils/constants.include.F'
  integer, intent(in)            :: a,c,n_pt
  double precision, intent(in)   :: B_10(n_pt_max_integrals),B_01(n_pt_max_integrals),B_00(n_pt_max_integrals)
  double precision, intent(out)  :: res(n_pt_max_integrals)
  double precision               :: res2(n_pt_max_integrals)
  integer                        :: i
  
  if(c<0)then
    do i=1,n_pt
      res(i) = 0.d0
    enddo
  else if (a==0) then
    call I_x2_new(c,B_10,B_01,B_00,res,n_pt)
  else if (a==1) then
    call I_x2_new(c-1,B_10,B_01,B_00,res,n_pt)
    do i=1,n_pt
      res(i) = c * B_00(i) * res(i)
    enddo
  else
    call I_x1_new(a-2,c,B_10,B_01,B_00,res,n_pt)
    call I_x1_new(a-1,c-1,B_10,B_01,B_00,res2,n_pt)
    do i=1,n_pt
      res(i) = (a-1) * B_10(i) * res(i) &
               + c * B_00(i) * res2(i)
    enddo
  endif
end
  
recursive subroutine I_x2_new(c,B_10,B_01,B_00,res,n_pt) 
  implicit none
  BEGIN_DOC
  !  recursive function involved in the bielectronic integral
  END_DOC
  include 'Utils/constants.include.F'
  integer, intent(in)            :: c, n_pt
  double precision, intent(in)   :: B_10(n_pt_max_integrals),B_01(n_pt_max_integrals),B_00(n_pt_max_integrals)
  double precision, intent(out)  :: res(n_pt_max_integrals)
  integer                        :: i
  
  if(c==1)then
    do i=1,n_pt
      res(i) = 0.d0
    enddo
  elseif(c==0) then
    do i=1,n_pt
      res(i) = 1.d0
    enddo
  else
    call I_x1_new(0,c-2,B_10,B_01,B_00,res,n_pt)
    do i=1,n_pt
      res(i) =  (c-1) * B_01(i) * res(i)
    enddo
  endif
end


integer function n_pt_sup(a_x,b_x,c_x,d_x,a_y,b_y,c_y,d_y,a_z,b_z,c_z,d_z)
  implicit none
  BEGIN_DOC
  ! Returns the upper boundary of the degree of the polynomial involved in the
  ! bielctronic integral :
  !       Ix(a_x,b_x,c_x,d_x) * Iy(a_y,b_y,c_y,d_y) * Iz(a_z,b_z,c_z,d_z)
  END_DOC
  integer                        :: a_x,b_x,c_x,d_x,a_y,b_y,c_y,d_y,a_z,b_z,c_z,d_z
  n_pt_sup =  ishft( a_x+b_x+c_x+d_x + a_y+b_y+c_y+d_y + a_z+b_z+c_z+d_z,1 )
end




subroutine give_polynom_mult_center_x(P_center,Q_center,a_x,d_x,p,q,n_pt_in,pq_inv,pq_inv_2,p10_1,p01_1,p10_2,p01_2,d,n_pt_out)
  implicit none
  BEGIN_DOC
  ! subroutine that returns the explicit polynom in term of the "t"
  ! variable of the following polynomw :
  !         I_x1(a_x, d_x,p,q) * I_x1(a_y, d_y,p,q) * I_x1(a_z, d_z,p,q)
  END_DOC
  integer, intent(in)            :: n_pt_in
  integer,intent(out)            :: n_pt_out
  integer, intent(in)            :: a_x,d_x
  double precision, intent(in)   :: P_center, Q_center
  double precision, intent(in)   :: p,q,pq_inv,p10_1,p01_1,p10_2,p01_2,pq_inv_2
  include 'Utils/constants.include.F'
  double precision,intent(out)   :: d(0:max_dim)
  double precision               :: accu
  accu = 0.d0
  ASSERT (n_pt_in >= 0)
  ! pq_inv = 0.5d0/(p+q)
  ! pq_inv_2 = 1.d0/(p+q)
  ! p10_1 = 0.5d0/p
  ! p01_1 = 0.5d0/q
  ! p10_2 = 0.5d0 *  q /(p * q + p * p)
  ! p01_2 = 0.5d0 *  p /(q * q + q * p)
  double precision               :: B10(0:2), B01(0:2), B00(0:2),C00(0:2),D00(0:2)
  B10(0)  = p10_1
  B10(1)  = 0.d0
  B10(2)  = - p10_2
  ! B10 = p01_1 - t**2 * p10_2
  B01(0)  = p01_1
  B01(1)  = 0.d0
  B01(2)  = - p01_2
  ! B01 = p01_1- t**2 * pq_inv
  B00(0)  = 0.d0
  B00(1)  = 0.d0
  B00(2)  = pq_inv
  ! B00 = t**2 * pq_inv
  do i = 0,n_pt_in
    d(i) = 0.d0
  enddo
  integer                        :: n_pt1,dim,i
  n_pt1 = n_pt_in
  ! C00 = -q/(p+q)*(Px-Qx) * t^2
  C00(0) = 0.d0
  C00(1) = 0.d0
  C00(2) =  -q*(P_center-Q_center) * pq_inv_2
  ! D00 = -p/(p+q)*(Px-Qx) * t^2
  D00(0) = 0.d0
  D00(1) = 0.d0
  D00(2) =  -p*(Q_center-P_center) * pq_inv_2
  !D00(2) =  -p*(Q_center(1)-P_center(1)) /(p+q)
  !DIR$ FORCEINLINE
  call I_x1_pol_mult(a_x,d_x,B10,B01,B00,C00,D00,d,n_pt1,n_pt_in)
  n_pt_out = n_pt1
  if(n_pt1<0)then
    n_pt_out = -1
    do i = 0,n_pt_in
      d(i) = 0.d0
    enddo
    return
  endif
  
end

subroutine I_x1_pol_mult(a,c,B_10,B_01,B_00,C_00,D_00,d,nd,n_pt_in)
  implicit none
  BEGIN_DOC
  ! recursive function involved in the bielectronic integral
  END_DOC
  integer , intent(in)           :: n_pt_in
  include 'Utils/constants.include.F'
  double precision,intent(inout) :: d(0:max_dim)
  integer,intent(inout)          :: nd
  integer, intent(in)            :: a,c
  double precision, intent(in)   :: B_10(0:2),B_01(0:2),B_00(0:2),C_00(0:2),D_00(0:2)
  if( (c>=0).and.(nd>=0) )then
    
    if (a==1) then
      call I_x1_pol_mult_a1(c,B_10,B_01,B_00,C_00,D_00,d,nd,n_pt_in)
    else if (a==2) then
      call I_x1_pol_mult_a2(c,B_10,B_01,B_00,C_00,D_00,d,nd,n_pt_in)
    else if (a>2) then
      call I_x1_pol_mult_recurs(a,c,B_10,B_01,B_00,C_00,D_00,d,nd,n_pt_in)
    else  ! a == 0
      
      if( c==0 )then
        nd = 0
        d(0) = 1.d0
        return
      endif
      
      call I_x2_pol_mult(c,B_10,B_01,B_00,C_00,D_00,d,nd,n_pt_in)
    endif
  else
    nd = -1
  endif
end

recursive subroutine I_x1_pol_mult_recurs(a,c,B_10,B_01,B_00,C_00,D_00,d,nd,n_pt_in)
  implicit none
  BEGIN_DOC
  ! recursive function involved in the bielectronic integral
  END_DOC
  integer , intent(in)           :: n_pt_in
  include 'Utils/constants.include.F'
  double precision,intent(inout) :: d(0:max_dim)
  integer,intent(inout)          :: nd
  integer, intent(in)            :: a,c
  double precision, intent(in)   :: B_10(0:2),B_01(0:2),B_00(0:2),C_00(0:2),D_00(0:2)
  double precision               :: X(0:max_dim)
  double precision               :: Y(0:max_dim)
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: X,Y
  integer                        :: nx, ix,iy,ny
  
  ASSERT (a>2)
  !DIR$ LOOP COUNT(8)
  do ix=0,n_pt_in
    X(ix) = 0.d0
  enddo
  nx = 0
  if (a==3) then
    call I_x1_pol_mult_a1(c,B_10,B_01,B_00,C_00,D_00,X,nx,n_pt_in)
  else if (a==4) then
    call I_x1_pol_mult_a2(c,B_10,B_01,B_00,C_00,D_00,X,nx,n_pt_in)
  else
    ASSERT (a>=5)
    call I_x1_pol_mult_recurs(a-2,c,B_10,B_01,B_00,C_00,D_00,X,nx,n_pt_in)
  endif
  
  !DIR$ LOOP COUNT(8)
  do ix=0,nx
    X(ix) *= dble(a-1)
  enddo
  
  !DIR$ FORCEINLINE
  call multiply_poly(X,nx,B_10,2,d,nd)
  
  nx = nd
  !DIR$ LOOP COUNT(8)
  do ix=0,n_pt_in
    X(ix) = 0.d0
  enddo
  
  if (c>0) then
    if (a==3) then
      call I_x1_pol_mult_a2(c-1,B_10,B_01,B_00,C_00,D_00,X,nx,n_pt_in)
    else
      ASSERT(a >= 4)
      call I_x1_pol_mult_recurs(a-1,c-1,B_10,B_01,B_00,C_00,D_00,X,nx,n_pt_in)
    endif
    if (c>1) then
      !DIR$ LOOP COUNT(8)
      do ix=0,nx
        X(ix) *= c
      enddo
    endif
    !DIR$ FORCEINLINE
    call multiply_poly(X,nx,B_00,2,d,nd)
  endif
  
  ny=0
  
  !DIR$ LOOP COUNT(8)
  do ix=0,n_pt_in
    Y(ix) = 0.d0
  enddo
  ASSERT(a > 2)
  if (a==3) then
    call I_x1_pol_mult_a2(c,B_10,B_01,B_00,C_00,D_00,Y,ny,n_pt_in)
  else
    ASSERT(a >= 4)
    call I_x1_pol_mult_recurs(a-1,c,B_10,B_01,B_00,C_00,D_00,Y,ny,n_pt_in)
  endif
  
  !DIR$ FORCEINLINE
  call multiply_poly(Y,ny,C_00,2,d,nd)
  
end

recursive subroutine I_x1_pol_mult_a1(c,B_10,B_01,B_00,C_00,D_00,d,nd,n_pt_in)
  implicit none
  BEGIN_DOC
  ! recursive function involved in the bielectronic integral
  END_DOC
  integer , intent(in)           :: n_pt_in
  include 'Utils/constants.include.F'
  double precision,intent(inout) :: d(0:max_dim)
  integer,intent(inout)          :: nd
  integer, intent(in)            :: c
  double precision, intent(in)   :: B_10(0:2),B_01(0:2),B_00(0:2),C_00(0:2),D_00(0:2)
  double precision               :: X(0:max_dim)
  double precision               :: Y(0:max_dim)
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: X,Y
  integer                        :: nx, ix,iy,ny
  
  if( (c<0).or.(nd<0) )then
    nd = -1
    return
  endif
  
  nx = nd
  !DIR$ LOOP COUNT(8)
  do ix=0,n_pt_in
    X(ix) = 0.d0
  enddo
  call I_x2_pol_mult(c-1,B_10,B_01,B_00,C_00,D_00,X,nx,n_pt_in)
  
  if (c>1) then
    !DIR$ LOOP COUNT(8)
    do ix=0,nx
      X(ix) *= dble(c)
    enddo
  endif
  
  !DIR$ FORCEINLINE
  call multiply_poly(X,nx,B_00,2,d,nd)
  
  ny=0
  
  !DIR$ LOOP COUNT(8)
  do ix=0,n_pt_in
    Y(ix) = 0.d0
  enddo
  call I_x2_pol_mult(c,B_10,B_01,B_00,C_00,D_00,Y,ny,n_pt_in)
  
  !DIR$ FORCEINLINE
  call multiply_poly(Y,ny,C_00,2,d,nd)
  
end

recursive subroutine I_x1_pol_mult_a2(c,B_10,B_01,B_00,C_00,D_00,d,nd,n_pt_in)
  implicit none
  BEGIN_DOC
  ! recursive function involved in the bielectronic integral
  END_DOC
  integer , intent(in)           :: n_pt_in
  include 'Utils/constants.include.F'
  double precision,intent(inout) :: d(0:max_dim)
  integer,intent(inout)          :: nd
  integer, intent(in)            :: c
  double precision, intent(in)   :: B_10(0:2),B_01(0:2),B_00(0:2),C_00(0:2),D_00(0:2)
  double precision               :: X(0:max_dim)
  double precision               :: Y(0:max_dim)
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: X,Y
  integer                        :: nx, ix,iy,ny
  
  !DIR$ LOOP COUNT(8)
  do ix=0,n_pt_in
    X(ix) = 0.d0
  enddo
  nx = 0
  call I_x2_pol_mult(c,B_10,B_01,B_00,C_00,D_00,X,nx,n_pt_in)
  
  !DIR$ FORCEINLINE
  call multiply_poly(X,nx,B_10,2,d,nd)
  
  nx = nd
  !DIR$ LOOP COUNT(8)
  do ix=0,n_pt_in
    X(ix) = 0.d0
  enddo
  
  !DIR$ FORCEINLINE
  call I_x1_pol_mult_a1(c-1,B_10,B_01,B_00,C_00,D_00,X,nx,n_pt_in)
  
  if (c>1) then
    !DIR$ LOOP COUNT(8)
    do ix=0,nx
      X(ix) *= dble(c)
    enddo
  endif
  
  !DIR$ FORCEINLINE
  call multiply_poly(X,nx,B_00,2,d,nd)
  
  ny=0
  !DIR$ LOOP COUNT(8)
  do ix=0,n_pt_in
    Y(ix) = 0.d0
  enddo
  !DIR$ FORCEINLINE
  call I_x1_pol_mult_a1(c,B_10,B_01,B_00,C_00,D_00,Y,ny,n_pt_in)
  
  !DIR$ FORCEINLINE
  call multiply_poly(Y,ny,C_00,2,d,nd)
  
end

recursive subroutine I_x2_pol_mult(c,B_10,B_01,B_00,C_00,D_00,d,nd,dim)
  implicit none
  BEGIN_DOC
  ! recursive function involved in the bielectronic integral
  END_DOC
  integer , intent(in)           :: dim
  include 'Utils/constants.include.F'
  double precision               :: d(0:max_dim)
  integer,intent(inout)          :: nd
  integer, intent(in)            :: c
  double precision, intent(in)   :: B_10(0:2),B_01(0:2),B_00(0:2),C_00(0:2),D_00(0:2)
  integer                        :: nx, ix,ny
  double precision               :: X(0:max_dim),Y(0:max_dim)
  !DIR$ ATTRIBUTES ALIGN : $IRP_ALIGN :: X, Y
  integer                        :: i
  
  select case (c)
    case (0)
      nd = 0
      d(0) = 1.d0
      return
      
    case (:-1)
      nd = -1
      return
      
    case (1)
      nd = 2
      d(0) = D_00(0)
      d(1) = D_00(1)
      d(2) = D_00(2)
      return
      
    case (2)
      nd = 2
      d(0) = B_01(0)
      d(1) = B_01(1)
      d(2) = B_01(2)
      
      ny = 2
      Y(0) = D_00(0)
      Y(1) = D_00(1)
      Y(2) = D_00(2)
      
      !DIR$ FORCEINLINE
      call multiply_poly(Y,ny,D_00,2,d,nd)
      return
      
      case default
      
      !DIR$ LOOP COUNT(6)
      do ix=0,c+c
        X(ix) = 0.d0
      enddo
      nx = 0
      call I_x2_pol_mult(c-2,B_10,B_01,B_00,C_00,D_00,X,nx,dim)
      
      !DIR$ LOOP COUNT(6)
      do ix=0,nx
        X(ix) *= dble(c-1)
      enddo
      
      !DIR$ FORCEINLINE
      call multiply_poly(X,nx,B_01,2,d,nd)
      
      ny = 0
      !DIR$ LOOP COUNT(6)
      do ix=0,c+c
        Y(ix) = 0.d0
      enddo
      call I_x2_pol_mult(c-1,B_10,B_01,B_00,C_00,D_00,Y,ny,dim)
      
      !DIR$ FORCEINLINE
      call multiply_poly(Y,ny,D_00,2,d,nd)
      
  end select
end


  

subroutine compute_union_aux_ao_integrals_jl(j,l,n_integrals,buffer_i,buffer_value)
  implicit none
  use map_module
  BEGIN_DOC
  !  Parallel client for AO integrals
  END_DOC
  
  integer, intent(in)            :: j,l
  integer,intent(out)            :: n_integrals
  integer(key_kind),intent(out)  :: buffer_i(union_aux_ao_num*union_aux_ao_num)
  real(integral_kind),intent(out) :: buffer_value(union_aux_ao_num*union_aux_ao_num)

  integer                        :: i,k
  double precision               :: union_aux_ao_bielec_integral,cpu_1,cpu_2, wall_1, wall_2
  double precision               :: integral, wall_0
  double precision               :: thr
  integer                        :: kk, m, j1, i1

  thr = union_aux_ao_integrals_threshold
  
  n_integrals = 0
  
  j1 = j+ishft(l*l-l,-1)
  do k = 1, union_aux_ao_num           ! r1
    i1 = ishft(k*k-k,-1)
    if (i1 > j1) then
      exit
    endif
    do i = 1, k
      i1 += 1
      if (i1 > j1) then
        exit
      endif
      if (union_aux_ao_overlap_abs(i,k)*union_aux_ao_overlap_abs(j,l) < thr) then
        cycle
      endif
      if (union_aux_ao_bielec_integral_schwartz(i,k)*union_aux_ao_bielec_integral_schwartz(j,l) < thr ) then
        cycle
      endif
      !DIR$ FORCEINLINE
      integral = union_aux_ao_bielec_integral(i,k,j,l)  ! i,k : r1    j,l : r2
      if (abs(integral) < thr) then
        cycle
      endif
      n_integrals += 1
      !DIR$ FORCEINLINE
      call bielec_integrals_index(i,j,k,l,buffer_i(n_integrals))
      buffer_value(n_integrals) = integral
    enddo
  enddo
    
end
