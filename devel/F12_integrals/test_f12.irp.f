program icule
  implicit none
  integer :: i,j,k,l
  double precision :: integral

  PROVIDE mo_F12_integrals_in_map
  double precision :: get_mo_F12_integral

  do i=1,mo_tot_num
    do j=1,mo_tot_num
      do k=1,mo_tot_num
        do l=1,mo_tot_num
          integral = get_mo_F12_integral(i,j,k,l,mo_F12_integrals_map)
          if (dabs(integral) > 0.d0) then
            print *,  i,j,k,l,integral
          endif
        enddo
      enddo
    enddo
  enddo
  return
end
