program bench_maps
  use map_module
  implicit none
  BEGIN_DOC
! Performs timing benchmarks on integral access
  END_DOC
  integer                        :: i,j,k,l
  integer*8                      :: ii,jj
  double precision               :: r, cpu
  integer*8                      :: cpu0, cpu1, count_rate, count_max
  
  PROVIDE mo_bielec_integrals_in_map

  do i=0,mo_integrals_map%map_size
    print *,  i, mo_integrals_map%map(i)%n_elements
    print '(32780(I6))',  (mo_integrals_map%map(i)%key(j), j=1,mo_integrals_map%map(i)%n_elements)
  enddo

end
