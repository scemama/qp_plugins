program print_integrals
  implicit none
  BEGIN_DOC
! Prints all two-electron integrals
  END_DOC
  integer                        :: i,j,k,l
  integer*8                      :: ii,jj
  double precision               :: r, cpu
  double precision               :: get_mo_bielec_integral, integral

  
  PROVIDE mo_bielec_integrals_in_map
  print *,  mo_tot_num, 'MOs'

  do l=1,mo_tot_num
    do k=1,mo_tot_num
      do j=1,mo_tot_num
        do i=1,mo_tot_num
          ii += 1
          integral = get_mo_bielec_integral(i,j,k,l,mo_integrals_map)
          if (dabs(integral) > 1.d-15) then
            print *,  i,j,k,l, integral
          endif
        enddo
      enddo
    enddo
  enddo

end
